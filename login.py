import apsw
import flask
from flask import request, render_template
from login_form import LoginForm
from apsw import Error

# Add a login manager to the app
import flask_login
from flask_login import login_user
login_manager = flask_login.LoginManager()
def login_manager_setup(app):
    login_manager.init_app(app)
    login_manager.login_view = "login"
    return login_manager
currUser = None
conn = apsw.Connection('./tiny.db')


# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    pass


def register(form):
    try:
        conn.execute('INSERT INTO users (username, password) VALUES (?,?);', (form.username.data, form.password.data))
    except Error as e:
        return ('ERROR: {e}')
    return render_template('./login.html', form=form)


# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(username):
    user = User()
    user.id = conn.execute('SELECT id FROM users WHERE username GLOB ?', [username])
    return user

def loginAttempt():
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        if form.createuser.data:
            register(form)

        if form.submit.data:
            username = form.username.data
            password = form.password.data
            attempt = [(username, password)]
            result = c.execute('SELECT username, password FROM users WHERE username GLOB ?', [username])
            user = result.fetchall()
            if attempt == user:
                user = user_loader(username)
                currUser = username
                # automatically sets logged in session cookie
                login_user(user)

                flask.flash('Logged in successfully.')

                next = flask.request.args.get('next')
                # is_safe_url should check if the url is safe for redirects.
                # See http://flask.pocoo.org/snippets/62/ for an example.
                if False and not is_safe_url(next):
                    return flask.abort(400)
                return (flask.redirect(next or flask.url_for('index_html')), username)
    return (render_template('./login.html', form=form), None)