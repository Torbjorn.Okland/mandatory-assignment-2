2B:
   *  I refractored the code to put login in related code in it's own file. This protects the user by keeping things seperate and unobtainable in the main app. As well as it keeps the programmer organized which reduces the risk of programming errors.
   
   I also removed the SQL injection vunrability by making the SQL statments into prepared statements.

   I also noted that the flask secret code was "mY s3kritz" and should be changed into something unguessable.

   *  My application allows for Login, Create User, Send Message, Recive Message and Search Message.

   *  Run Flask
      Go to http://127.0.0.1:5000/login
      Create a User
      Login with said User
      Send or Search for a message
      Recived messages will appear for every action done.

   *  Who might attack the application?
      *  Bored Teens
      *  Ignorant Hackers looking for fun
      *  Person with malicious intent
      What can an attacker do?
      *  Leak passwords
      *  Inject or destroy data
      *  Impersonate others
      *  Blackmail a person with information gained
      Are there limits to what an attacker can do?
      *  The system does not contain private information like address, full name, social security number.
      Are there limits to what we can sensibly protect against?
      *  Cyber security is an ever changing field. For every solution to an exploit a new one will arrise. An ever ongoing battle between hacker and programmer.
   
   *  What are the main attack vectors for the application?
      As listed in the assignment the main attack vectors are: 
      *  SQL injections
      *  Cross-site scripting attacks
      *  Cross-site request forgery
   
   *  What should we do (or what have you done) to protect against attacks?
      *  Turned the SQL statments into prepared statments, which should prevent the most basic SQL injections.
   
   *
